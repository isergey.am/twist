#pragma once

#include <twist/rt/layer/strand/stdlike/mutex.hpp>
#include <twist/rt/layer/strand/stdlike/condvar.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>

namespace twist::test {

// Allows one or more threads to wait until a set of operations being performed
// in other threads completes

class CountDownLatch {
 public:
  CountDownLatch(size_t count) : count_(count) {
  }

  // Decrements the count of the latch, releasing all waiting threads if the
  // count reaches zero

  void CountDown() {
    std::lock_guard guard(mutex_);
    --count_;
    if (count_ == 0) {
      released_.notify_all();
    }
  }

  // Wait until the latch has counted down to zero

  void Await() {
    std::unique_lock lock(mutex_);
    while (count_ > 0) {
      released_.wait(lock);
    }
  }

 private:
  size_t count_;
  rt::strand::stdlike::mutex mutex_;
  rt::strand::stdlike::condition_variable released_;
};

}  // namespace twist::test
