#pragma once

#include <twist/rt/run/env.hpp>

namespace twist::test {

rt::IEnv* WheelsTestEnv();

}  // namespace twist::test
