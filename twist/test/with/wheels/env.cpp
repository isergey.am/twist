#include "stress.hpp"

#include <twist/rt/run/env.hpp>

#include <iostream>

namespace twist::test {

struct WheelsTestEnv : rt::IEnv {
  size_t Seed() const override {
    return wheels::test::TestHash();
  }

  bool KeepRunning() const override {
    static const auto kSafeMargin = 500ms;
    return wheels::test::TestTimeLeft() > kSafeMargin;
  }

  void PrintLine(std::string message) override {
    std::cout << message << std::endl;
  }

  void Exception() override {
    wheels::test::FailTestByException();
  }

  void Assert(wheels::SourceLocation where, std::string reason) override {
    wheels::test::FailTestByAssert({reason, where});
  }
};

rt::IEnv* WheelsTestEnv() {
  static struct WheelsTestEnv instance;
  return &instance;
}

}  // namespace twist::test
