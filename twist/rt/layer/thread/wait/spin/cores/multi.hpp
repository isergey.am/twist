#pragma once

#include <cstdlib>
#include <thread>

namespace twist::rt::thread {

namespace cores::multi {

class [[nodiscard]] SpinWait {
 public:
  void SpinOnce();

  void operator()() {
    SpinOnce();
  }

 private:
  size_t spins_{0};
};

}  // namespace cores::multi

}  // namespace twist::rt::thread
