#include <twist/rt/layer/thread/wait/sys/wait.hpp>

#include <twist/rt/layer/thread/wait/sys/platform/wait.hpp>

namespace twist::rt::thread {

static uint32_t* PlatformAddr(std::atomic<uint32_t>& atomic) {
  return (uint32_t*)&atomic;
}

void Wait(std::atomic<uint32_t>& atomic, uint32_t old) {
  while (atomic.load() == old) {
    PlatformWait(PlatformAddr(atomic), old);
  }
}

WakeKey PrepareWake(std::atomic<uint32_t>& atomic) {
  return {PlatformAddr(atomic)};
}

static std::chrono::milliseconds ToMillis(std::chrono::nanoseconds ns) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(ns);
}

bool WaitTimed(std::atomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout) {
  using Clock = std::chrono::steady_clock;

  const auto deadline = Clock::now() + timeout;

  while (atomic.load() == old) {
    auto now = Clock::now();

    if (now >= deadline) {
      return false;
    }

    // deadline > now
    std::chrono::milliseconds left = ToMillis(deadline - now);
    PlatformWaitTimed(PlatformAddr(atomic), old, /*millis=*/left.count() + 1);
  }

  return true;
}

void WakeOne(WakeKey key) {
  PlatformWake(key.addr, 1);
}

void WakeAll(WakeKey key) {
  PlatformWake(key.addr, 100500);
}

}  // namespace twist::rt::thread
