#pragma once

#include <twist/rt/layer/strand/local/tls.hpp>

#include <wheels/core/noncopyable.hpp>

namespace twist::rt::strand {

//////////////////////////////////////////////////////////////////////

/*
 * Usage:
 *
 * ThreadLocal<int> value{256};
 *
 * *value = 1024;  // store
 * int this_thread_value = *value;  // load
 *
 */

template <typename T>
class ThreadLocal : private wheels::NonCopyable {
 public:
  ThreadLocal(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    Init({ctor, dtor});
  }

  explicit ThreadLocal(T default_value) {
    InitWithConstructor([default_value]() -> void* {
      return new T(default_value);
    });
  }

  explicit ThreadLocal() {
    InitWithConstructor([]() -> void* {
      return new T;
    });
  }

  T* Get() {
    return GetPointer();
  }

  // Access thread-local instance
  T& operator*() {
    return *GetPointer();
  }

  // Usage: thread_local_obj->Method();
  T* operator->() {
    return GetPointer();
  }

 private:
  void Init(TLSManager::Var var) {
    slot_index_ = TLSManager::Instance().AcquireSlot(std::move(var));
  }

  void InitWithConstructor(TLSManager::Ctor ctor) {
    auto dtor = [](void* ptr) {
      delete (T*)ptr;
    };

    Init({ctor, dtor});
  }

  T* GetPointer() {
    Slot* slot = TLSManager::Instance().Access(slot_index_);
    T* data = slot->LoadAs<T>();
    WHEELS_VERIFY(data != nullptr, "Not initialized");
    return data;
  }

 private:
  size_t slot_index_;
};

}  // namespace twist::rt::strand
