#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/layer/fiber/stdlike/condvar.hpp>

namespace twist::rt::strand::stdlike {

using condition_variable = fiber::CondVar;  // NOLINT

}  // namespace twist::rt::strand::stdlike

#else

// native threads

#include <condition_variable>

namespace twist::rt::strand::stdlike {

using ::std::condition_variable;

}  // namespace twist::rt::strand::stdlike

#endif
