#pragma once

#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>

#include <wheels/core/assert.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>

namespace twist::rt::fiber {

class Mutex {
 public:
  void Lock() {
    while (locked_) {
      wait_queue_.Park();
    }
    locked_ = true;
  }

  bool TryLock() {
    return !std::exchange(locked_, true);
  }

  void Unlock() {
    WHEELS_VERIFY(locked_, "Unlocking mutex that is not locked");
    locked_ = false;
    wait_queue_.WakeOne();
  }

  // std::mutex / Lockable

  void lock() {  // NOLINT
    Lock();
  }

  bool try_lock() {  // NOLINT
    return TryLock();
  }

  void unlock() {  // NOLINT
    Unlock();
  }

 private:
  bool locked_{false};
  WaitQueue wait_queue_{"mutex::lock"};
};

}  // namespace twist::rt::fiber
