#pragma once

#include <twist/rt/layer/fiber/runtime/time.hpp>

namespace twist::rt::fiber {

class SteadyClock {
 public:
  using rep = VirtualTime::rep;
  using period = VirtualTime::period;
  using duration = VirtualTime::duration;
  using time_point = VirtualTime::time_point;
  static const bool is_steady = true;

 public:
  static time_point now();
};

}  // namespace twist::rt::fiber
