#pragma once

#include <twist/rt/layer/fiber/runtime/syscalls.hpp>

namespace twist::rt::fiber {

class [[nodiscard]] SpinWait {
 public:
  void SpinOnce() {
    Yield();
  }

  void operator()() {
    SpinOnce();
  }
};

inline void CpuRelax() {
  Yield();
}

}  // namespace twist::rt::fiber
