#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

uint64_t GenerateRandomUInt64();

}  // namespace twist::rt::fiber
