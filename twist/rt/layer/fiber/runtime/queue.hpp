#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>

#if defined(TWIST_FAST_FIBER_QUEUES)

#include <twist/rt/layer/fiber/runtime/detail/deque/fixed_size.hpp>
#include <twist/rt/layer/fiber/runtime/detail/deque/std.hpp>

namespace twist::rt::fiber {

// Deque-based fiber queue with fast O(1) PickRandom

class FiberQueue {
 public:
  void PushBack(Fiber* f);

  bool IsEmpty() const;

  Fiber* PopFront();
  // For fault injection
  Fiber* PopRandom();

 private:
  static const size_t kFiberLimit = 64;
  using FiberDeque = detail::PtrFixedSizeDeque<Fiber, kFiberLimit>;

  FiberDeque impl_;
};

}  // twist::rt::fiber

#else

#include <wheels/intrusive/list.hpp>

namespace twist::rt::fiber {

// Allocation-free IntrusiveList-based fiber queue

class FiberQueue {
 public:
  void PushBack(Fiber* f);

  bool IsEmpty() const;

  Fiber* PopFront();

  // For fault injection
  Fiber* PopRandom();

 private:
  wheels::IntrusiveList<Fiber> impl_;
};

}  // namespace twist::rt::fiber

#endif
