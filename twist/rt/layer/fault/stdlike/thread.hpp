#pragma once

#include <twist/rt/layer/strand/stdlike/thread.hpp>

#include <function2/function2.hpp>

namespace twist::rt {
namespace fault {

//////////////////////////////////////////////////////////////////////

class FaultyThread {
  using ThreadRoutine = fu2::unique_function<void()>;
 public:
  FaultyThread(ThreadRoutine routine);

  // NOLINTNEXTLINE
  void join();

  // NOLINTNEXTLINE
  bool joinable() const {
    return impl_.joinable();
  }

  // NOLINTNEXTLINE
  void detach() {
    impl_.detach();
  }

 private:
  strand::stdlike::thread impl_;
};

//////////////////////////////////////////////////////////////////////

namespace this_thread {

// No fault injection needed
using strand::stdlike::this_thread::get_id;
using strand::stdlike::this_thread::yield;
using strand::stdlike::this_thread::sleep_for;

}  // namespace this_thread

}  // namespace fault
}  // namespace twist::rt
