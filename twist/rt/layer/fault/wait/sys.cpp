#include <twist/rt/layer/fault/wait/sys.hpp>

#include <twist/rt/layer/strand/wait/sys.hpp>

namespace twist::rt::fault {

using WaitableAtomic = FaultyAtomic<uint32_t>;

WaitableAtomic::Impl& Impl(WaitableAtomic& atomic) {
  static_assert(sizeof(WaitableAtomic) == sizeof(WaitableAtomic::Impl));

  return reinterpret_cast<WaitableAtomic::Impl&>(atomic);
}


void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old) {
  strand::Wait(Impl(atomic), old);
}

bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout) {
  return strand::WaitTimed(Impl(atomic), old, timeout);
}

WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic) {
  return strand::PrepareWake(Impl(atomic));
}

void WakeOne(WakeKey key) {
  strand::WakeOne(key);
}

void WakeAll(WakeKey key) {
  strand::WakeAll(key);
}

}  // namespace twist::rt::fault
