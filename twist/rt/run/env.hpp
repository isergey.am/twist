#pragma once

#include <wheels/core/source_location.hpp>

#include <cstdlib>
#include <string>

namespace twist::rt {

struct IEnv {
  virtual ~IEnv() = default;

  // Seed for PRNG
  virtual size_t Seed() const = 0;

  // Time budget
  virtual bool KeepRunning() const = 0;

  virtual void PrintLine(std::string message) = 0;

  // Fail

  virtual void Exception() = 0;

  virtual void Assert(wheels::SourceLocation where,
                      std::string reason) = 0;
};

}  // namespace twist::rt
